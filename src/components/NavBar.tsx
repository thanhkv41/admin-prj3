"use client";
import {
  Center,
  Image,
  Stack,
  Tooltip,
  UnstyledButton,
  rem,
} from "@mantine/core";
import {
  IconCalendarStats,
  IconDeviceDesktopAnalytics,
  IconGauge,
  type IconHome2,
  IconLogout,
  IconSwitchHorizontal,
} from "@tabler/icons-react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import classes from "./Navbar.module.css";

interface NavbarLinkProps {
  icon: typeof IconHome2;
  label: string;
  active?: boolean;
  onClick?(): void;
}

function NavbarLink({ icon: Icon, label, active, onClick }: NavbarLinkProps) {
  return (
    <Tooltip label={label} position="right" transitionProps={{ duration: 0 }}>
      <UnstyledButton
        onClick={onClick}
        className={classes.link}
        data-active={active ? true : undefined}
      >
        <Icon style={{ width: rem(20), height: rem(20) }} stroke={1.5} />
      </UnstyledButton>
    </Tooltip>
  );
}

const mockdata = [
  { icon: IconGauge, label: "Dashboard", route: "/dashboard" },
  { icon: IconDeviceDesktopAnalytics, label: "Analytics", route: "/analytics" },
  { icon: IconCalendarStats, label: "Releases", route: "/releases" },
];

export function Navbar() {
  const [active, setActive] = useState(0);
  const router = useRouter();
  const links = mockdata.map((link, index) => {
    return (
      <NavbarLink
        {...link}
        key={link.label}
        active={index === active}
        onClick={() => {
          setActive(index);
          router.push(link.route);
        }}
      />
    );
  });

  return (
    <nav className={classes.navbar}>
      <Center>
        <Image h={50} w="auto" fit="contain" src="/logo.png" />
      </Center>

      <div className={classes.navbarMain}>
        <Stack justify="center" gap={0}>
          {links}
        </Stack>
      </div>

      <Stack justify="center" gap={0}>
        <NavbarLink icon={IconSwitchHorizontal} label="Change account" />
        <NavbarLink icon={IconLogout} label="Logout" />
      </Stack>
    </nav>
  );
}
