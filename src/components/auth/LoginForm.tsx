"use client";

import useNotify, { Variant } from "@/hooks/useNotify";
import {
  Button,
  LoadingOverlay,
  PasswordInput,
  Stack,
  TextInput,
} from "@mantine/core";
import { isEmail, useForm } from "@mantine/form";
import { getCsrfToken, signIn, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";

const LoginForm = () => {
  const [isSubmiting, setIsSubmiting] = useState(false);
  const form = useForm({
    initialValues: {
      email: "",
      password: "",
    },
    validate: {
      email: isEmail("Email không hợp lệ"),
      password: (value: string) =>
        value.length === 0
          ? "Vui lòng nhập mật khẩu"
          : /\s/.test(value)
            ? "Mật khẩu không được chứa ký tự khoảng trắng"
            : null,
    },
  });
  const { notify } = useNotify();
  const router = useRouter();
  const { status } = useSession();

  const handleSubmit = async (values: { email: string; password: string }) => {
    const { email, password } = values;
    const crsfToken = await getCsrfToken();
    setIsSubmiting(true);

    const response = await signIn("credentials", {
      email,
      password,
      crsfToken,
      redirect: false,
      callbackUrl: "/",
    });

    form.reset();

    if (!response?.ok) {
      if (response?.error) {
        notify(response?.error, Variant.Error);
      }
    }

    if (response?.ok) {
      router.push("/dashboard");
    }

    setIsSubmiting(false);
  };

  useEffect(() => {
    if (status === "authenticated") {
      router.push("/dashboard");
    }
  }, []);

  return (
    <>
      <LoadingOverlay visible={isSubmiting} />
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Stack gap={0}>
          <TextInput
            label="Email"
            placeholder="you@mantine.dev"
            {...form.getInputProps("email")}
          />
          <PasswordInput
            {...form.getInputProps("password")}
            label="Mật khẩu"
            placeholder="Mật khẩu của bạn"
            mt="md"
          />
          <Button fullWidth mt="xl" type="submit">
            Đăng nhập
          </Button>
        </Stack>
      </form>
    </>
  );
};

export default LoginForm;
