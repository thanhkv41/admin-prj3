import { PrismaAdapter } from "@next-auth/prisma-adapter";
import {
  getServerSession,
  type DefaultSession,
  type NextAuthOptions,
} from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { compare } from "bcryptjs";

import { db } from "@/server/db";
import { api } from "@/trpc/server";

export async function verifyPassword(password: string, hashedPassword: string) {
  return await compare(password, hashedPassword);
}

/**
 * Module augmentation for `next-auth` types. Allows us to add custom properties to the `session`
 * object and keep type safety.
 *
 * @see https://next-auth.js.org/getting-started/typescript#module-augmentation
 */
declare module "next-auth" {
  interface Session extends DefaultSession {
    user: {
      id: string;
      // ...other properties
      // role: UserRole;
    } & DefaultSession["user"];
  }

  // interface User {
  //   // ...other properties
  //   // role: UserRole;
  // }
}

/**
 * Options for NextAuth.js used to configure adapters, providers, callbacks, etc.
 *
 * @see https://next-auth.js.org/configuration/options
 */
export const authOptions: NextAuthOptions = {
  adapter: PrismaAdapter(db),
  pages: {
    signIn: "/dashboard",
  },
  session: {
    strategy: "jwt",
  },
  providers: [
    CredentialsProvider({
      id: "credentials",
      credentials: {
        email: {
          type: "email",
        },
        password: { type: "password" },
      },
      async authorize(credentials) {
        if (!credentials) {
          throw new Error("Không tìm thấy thông tin đăng nhập nào.");
        }
        const { email, password } = credentials;

        if (!email || !password) {
          return null;
        }

        const user = await api.user.getByEmail.query({ email });

        if (!user) {
          throw new Error("Thông tin đăng nhập không hợp lệ");
        }

        const hasValidPassword = await verifyPassword(password, user?.password);

        if (!hasValidPassword) {
          throw new Error("Thông tin đăng nhập không hợp lệ");
        }

        return {
          id: user.id,
          name: user.name,
          email: user.email,
        };
      },
    }),
  ],
  secret: process.env.NEXTAUTH_SECRET,
  callbacks: {
    session: ({ session, token }) => {
      if (token && session) {
        session.user.id = token.sub!;
      }

      return session;
    },
    jwt: ({ token, user, trigger, session }) => {
      if (trigger === "update" && session?.user.name) {
        const updateUsername = { ...user, name: session.user.name };
        return { ...token, ...updateUsername };
      }
      return { ...token, ...user };
    },
  },
};

/**
 * Wrapper for `getServerSession` so that you don't need to import the `authOptions` in every file.
 *
 * @see https://next-auth.js.org/configuration/nextjs
 */
export const getServerAuthSession = () => getServerSession(authOptions);
