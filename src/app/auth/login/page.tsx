import { AuthProvider } from "@/components/auth/AuthProvider";
import LoginForm from "@/components/auth/LoginForm";
import { getServerAuthSession } from "@/server/auth";
import { Paper, Title } from "@mantine/core";

const LoginPage = async () => {
  const session = await getServerAuthSession();

  return (
    <AuthProvider session={session}>
      <Paper withBorder shadow="md" p={30} mt={30} radius="md">
        <Title ta="center" order={3}>
          Ủm Shop Administrator
        </Title>
        <LoginForm />
      </Paper>
    </AuthProvider>
  );
};

export default LoginPage;
