import { Container } from "@mantine/core";
import { PropsWithChildren } from "react";

export default async function RootLayout({ children }: PropsWithChildren) {
  return (
    <Container size={420} my={80}>
      {children}
    </Container>
  );
}
