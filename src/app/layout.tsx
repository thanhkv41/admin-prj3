import { ColorSchemeScript, MantineProvider } from "@mantine/core";
import { ModalsProvider } from "@mantine/modals";
import { Notifications } from "@mantine/notifications";
import { Inter } from "next/font/google";
import { type PropsWithChildren } from "react";
import { theme } from "./theme";
import "./globals.css";
import "@mantine/core/styles.css";
import "@mantine/notifications/styles.css";

const inter = Inter({
  subsets: ["latin"],
  variable: "--font-sans",
});

export const metadata = {
  title: "Admin page",
  description: "Trang quản trị của Ủm Shop",
  icons: [{ rel: "icon", url: "/favicon.ico" }],
};

export default function RootLayout({ children }: PropsWithChildren) {
  return (
    <html lang="en">
      <head>
        <ColorSchemeScript />
        <link rel="shortcut icon" href="/favicon.ico" />
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, user-scalable=no"
        />
      </head>
      <body className={`font-sans ${inter.variable}`}>
        <MantineProvider theme={theme}>
          <Notifications autoClose={5000} />
          <ModalsProvider labels={{ confirm: "Xác nhận", cancel: "Hủy bỏ" }}>
            {children}
          </ModalsProvider>
        </MantineProvider>
      </body>
    </html>
  );
}
