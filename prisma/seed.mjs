import { PrismaClient } from "@prisma/client";
import pkg from "bcryptjs";

const prisma = new PrismaClient();
const { hash } = pkg;
async function main() {
  const password = await hash("admin", 12);
  const user = await prisma.user.upsert({
    where: { email: "admin@admin.com" },
    update: {},
    create: {
      email: "admin@admin.com",
      name: "AnPhat Admin",
      password,
      emailVerified: new Date(),
    },
  });
  console.log({ user });
}
main()
  .then(() => prisma.$disconnect())
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
